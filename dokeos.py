# -*- coding: utf-8 -*-

import os
import re
import requests
from bs4 import BeautifulSoup
from config import username, password, planning_URL
from datetime import datetime
import locale


sess = requests.Session()
sess.post(
    planning_URL,
    data={
        'login': username,
        'password': password,
        'submitAuth': None,
        '_qf__formLogin': None
    })


def get_list():
    html = sess.get(planning_URL).text
    soup = BeautifulSoup(html, 'lxml')
    urls = []
    locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
    for a in soup.findAll('a', attrs={"style": u"float:left"}):
        date_found = re.findall('(au [0-9]+ [a-z]+ [0-9]+)[A-Za-z0-9\s]*\.pdf', a.text)
        if len(date_found) > 0:
            end_date = date_found[0]
            month = end_date.split(' ')[2]
            new_month = month.replace(month[0], month[0].upper())
            end_date = end_date.replace(month, new_month)
            end_date = end_date.replace(u'Fevrier', u'Février')
            end_date = end_date.replace(u'Decembre', u'Décembre')
            dtend = datetime.strptime(end_date.encode('utf-8'), "au %d %B %Y")
            if (dtend - datetime.now()).days >= 0:
                urls.append(a['href'])
    return urls
        
def download_file(url, path):
    r = sess.get(url)
    final_path = os.path.abspath(path + url.split('/')[-1].split('?')[0])
    with open(final_path, 'wb') as f:
        f.write(r.content)
    return final_path
