# -*- coding: utf-8 -*-


def select_group(df, group):
    default_regex = u'Promotion entière'
    if len(group) == 2:
        regex = 'Groupe .*' + group[0] + '|' + group + '|' + default_regex
    else:
        regex = 'Grpe MFE ' + group
    return df.loc[df['Groupe'].str.contains(regex)]


def week_split(df):
    days = df.loc[~df['...'].isin(["TP", "TPG", "TD", "CM", "EVAL."])]
    week = {}
    indexes = []
    for index, row in days.iterrows():
        indexes.append(index)
    for i in range(0, len(indexes) - 2):
        if indexes[i] < indexes[i + 1] - 1:
            week[df.loc[indexes[i], '...']] = df.iloc[indexes[i] +
                                                      1:indexes[i + 1], :]
    return week


def day_split(df):
    day = {}
    for index, row in df.iterrows():
        if row['Heure'] in day.keys():
            day[row['Heure']].append(row)
        else:
            day[row['Heure']] = [row]
    return day


def get_calendar(df, group):
    cal = {}
    week = week_split(df)
    for day, rows in week.iteritems():
        cal[day] = day_split(select_group(rows, group))
    return cal
