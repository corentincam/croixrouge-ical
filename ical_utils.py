# -*- coding: utf-8 -*-

import re
from icalendar import Calendar, Event
from datetime import datetime
import locale


def cal_to_ical(cal):
    ical = Calendar()
    locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
    for day, time in cal.iteritems():
        for hours, cal_events in time.iteritems():
            try:
                tstart = hours.split('\r')[0]
                tend = hours.split('\r')[1]
                dtstart = datetime.strptime((day + ' ' + tstart).encode('utf-8'),
                                            "%A %d %B %Y %H:%M")
                dtend = datetime.strptime((day + ' ' + tend).encode('utf-8'),
                                          "%A %d %B %Y %H:%M")
                for cal_event in cal_events:
                    event = Event()
                    summary = cal_event.iloc[0] + ' ' + ' '.join(list(set(cal_event['Mod/UE'].split('\r'))))
                    if cal_event['PO'] == 'F':
                        summary += ' (Facultatif)'
                    event.add('summary', summary)
                    event.add('dtstart', dtstart)
                    event.add('dtend', dtend)
                    event.add(
                        'description',
                        cal_event[u'Intitulé'].replace('\r', ' ').replace(
                            '\n', ' '))
                    if unicode(cal_event[u'Salle']) == 'nan':
                        event.add('location', 'Distanciel')
                    else:
                        rooms = re.findall(r'Salle\s?([A-Z]\s?[0-9]+)',
                                           unicode(cal_event[u'Salle']))
                        rooms = [room.replace(u' ', u'') for room in rooms]
                        rooms = ', '.join(rooms)
                        event.add('location', rooms)
                    ical.add_component(event)
            except Exception:
                pass
    return ical.to_ical()
